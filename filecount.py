import string
from operator import itemgetter


def getCloud():
    songLookup = {
        "closer" : "closer.txt",
        "happy" : "happy.txt",
        "power" : "power.txt",
        "we will rock you" : "weWillRockYou.txt",
        "shape of you" : "shapeOfYou.txt"
    }
    
    worddict = {}
    
    while True:
        print("Song list:")
        print()
        print("Power by Kanye West\nCloser by The Chainsmokers\nHappy by Pharrell Williams\nShape of You by Ed Sheeran\nWe Will Rock You by Queen")
        print()
        song = input("Please enter a song title from the list above: ")
        song = song.lower()
        if song in songLookup:
            break
        else:
            print(f"{song} is not in the song directory. Please enter another song")

    with open(songLookup[song], "r", encoding = 'utf-8') as f: 
        
        for line in f: 
            
            line = line.strip()
            for punc in string.punctuation:
                line = line.replace(punc, "")
                
            for word in line.split():
                word = word.lower()
                if word not in worddict:
                    worddict[word] = 1
                else:
                    worddict[word] += 1
                   
                    
    return(song ,list(worddict.items()))


        
    

        
        



