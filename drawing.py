from turtle import *
from filecount import *
speed(0)
song, wordList = getCloud()
penup()
x = -100
y = 250
goto(x,y)
write(song,font=("Arial", 24, "bold"), move=False)
x = -500
y = 200
goto(x,y)

for i in wordList:
    if i[1] <= 2:
        pencolor("lightskyblue4")
    elif i[1] <= 4:
        pencolor("lightskyblue3")
    elif i[1] <= 6:
        pencolor("lightskyblue2")
    elif i[1] <= 8:
        pencolor("lightskyblue1")
    elif i[1] <= 12:
        pencolor("lightskyblue")
    else:
        pencolor("dodgerblue")

    write(i[0],font=("Arial", i[1]+10, "normal"), move=True)
    forward(10)
    x_position = pos()[0]
    if x_position >= 450:
        y -= 50
        goto(x,y)

done()
