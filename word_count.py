words = {

}
phrase = "Cause the players gonna play, play, play, play, play And the haters gonna hate, hate, hate, hate, hate Baby, I'm just gonna shake, shake, shake, shake, shake I shake it off, I shake it off Heartbreakers gonna break, break, break, break, break And the fakers gonna fake, fake, fake, fake, fake Baby, I'm just gonna shake, shake, shake, shake, shake I shake it off, I shake it off I shake it off, I shake it off I, I shake it off, I shake it off I, I shake it off, I shake it off I, I shake it off, I shake it off"
punc = ''',.?!":;-'''    # common punctuation marks, saved into a string
for char in phrase:  	# go through each character in the "phrase" string
    if char in punc: 	# for each character in the string, checks if the character is also in the string called 'punc'
        phrase = phrase.replace(char, "") 	# replaces the character with nothing and resaves the string
s = "I shake it off"
splitPhrase = phrase.split()

count = 0
for item in splitPhrase:
    item = item.lower()
    words.setdefault(item,0)
    words[item]+=1

for key, value in words.items():
    print(f"{key}: {value}")    
